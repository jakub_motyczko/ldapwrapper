#include "LdapWrapper.h"

#include <QDebug>

#include "ldap.h"

LdapWrapper::LdapWrapper(const QByteArray &host)
    : mHost(host)
{
}

bool LdapWrapper::init(const QByteArray &base, const Scope scope, const QByteArray &scheme)
{
    if (mConnected) {
        qWarning() << "Ldap connected. Please disconnect first";
        return false;
    }
    mConnected = false;
    mBase = base;
    mScheme = scheme;
    mScope = scope;

    QByteArray connectionUrl = this->connectionUrl();

    int returnCode = ldap_initialize(&mConnection, connectionUrl.data());
    if (returnCode == LDAP_SUCCESS) {
        return true;
    }

    int protocol = 3;
    ldap_set_option(mConnection, LDAP_OPT_PROTOCOL_VERSION, &protocol);
    ldap_set_option(mConnection, LDAP_OPT_REFERRALS, nullptr);

    struct timeval networkTimeout;
    networkTimeout.tv_usec = 0;
    networkTimeout.tv_sec = 5;
    ldap_set_option(mConnection, LDAP_OPT_NETWORK_TIMEOUT, (void *)&networkTimeout);

    qWarning() << "ldap_initialize error:" << returnCode;
    return false;
}

bool LdapWrapper::connect(const QByteArray &login, const QByteArray &password, bool *ok)
{
    *ok = false;
    Q_ASSERT_X(mConnection != nullptr, "LDAP::connect", "Connection can't be null. Run init() first");

    if (mConnected) {
        qWarning() << "Ldap connected. Please disconnect first";
        return false;
    }

    mLogin = login;
    mPassword = password;

    LDAPControl	*serverControlsPtr[2] = {nullptr, nullptr};
    LDAPControl	serverControls[1];

    char policyRequest[] = LDAP_CONTROL_PASSWORDPOLICYREQUEST;
    LDAPControl control;
    bzero(&control, sizeof(control));
    control.ldctl_oid = policyRequest;
    serverControls[0] = control;
    serverControlsPtr[0] = &serverControls[0];

    int messageId = 0;

    struct berval passwordPtr = { (uint)mPassword.size(), mPassword.data() };
    int returnCode = ldap_sasl_bind(mConnection, mLogin.data(), LDAP_SASL_SIMPLE, &passwordPtr, serverControlsPtr, NULL, &messageId);
    if (messageId == -1) {
        qWarning() << "ldap_sasl_bind" << messageId << returnCode;
        return false;
    }

    LDAPMessage *result = nullptr;
    returnCode = ldap_result(mConnection, messageId, LDAP_MSG_ALL, NULL, &result);
    if (returnCode == -1 || returnCode == 0 || result == nullptr) {
        qWarning() << "ldap_result" << returnCode << result;

        if (returnCode == LDAP_INVALID_CREDENTIALS) {
            *ok = true;
        }

        if (result != nullptr) {
            ldap_msgfree(result);
        }
        return false;
    }

    LDAPControl **resultControls = nullptr;
    char *matcheddnp = nullptr;
    char *errorMessage = nullptr;
    char **referralsp = nullptr;

    int errorCode = 0;

    returnCode = ldap_parse_result(mConnection, result, &errorCode, &matcheddnp, &errorMessage, &referralsp, &resultControls, 1);
    if (matcheddnp != nullptr) {
        ldap_memfree(matcheddnp);
    }
    if (errorMessage != nullptr) {
        ldap_memfree(errorMessage);
    }
    if (referralsp != nullptr) {
        ldap_memvfree((void**)referralsp);
    }
    if (resultControls != nullptr) {
        ldap_controls_free(resultControls);
    }

    *ok = true;
    if (returnCode == LDAP_SUCCESS && errorCode == 0) {
        mConnected = true;
        return true;
    }
    qWarning() << "ldap_parse_result" << returnCode << errorCode;

    return false;
}

bool LdapWrapper::disconnect()
{
    Q_ASSERT_X(mConnection != nullptr, "LDAP::connect", "Connection can't be null. Run init() first");

    if (!mConnected) {
        qWarning() << "Ldap already disconnected. Please connect first";
        return false;
    }

    int returnCode = ldap_unbind_ext_s(mConnection, nullptr, nullptr);
    if (returnCode == LDAP_SUCCESS) {
        mConnected = false;
        mConnection = nullptr;
        return true;
    }

    return false;
}

bool LdapWrapper::search(const QByteArray &filter, QVector<Entry> *entryList)
{
    Q_ASSERT_X(mConnection != nullptr, "LDAP::connect", "Connection can't be null. Run init() first");

    if (!mConnected) {
        qWarning() << "Not connected. Aborting...";
        return false;
    }

    LDAPMessage *result = nullptr;
    int returnCode = ldap_search_ext_s(mConnection,
                                   mBase.data(),
                                   (int)mScope,
                                   filter.constData(),
                                   NULL,
                                   0,
                                   NULL,
                                   NULL,
                                   NULL,
                                   0,
                                   &result);

    if (returnCode != LDAP_SUCCESS ) {
        qWarning() << "ldap_search_ext_s error:" << returnCode << ldap_err2string(returnCode);
        return false;
    }

    //parse result
    LDAPMessage *message = nullptr;
    for (
         message = ldap_first_entry(mConnection, result);
         message != nullptr;
         message = ldap_next_entry(mConnection, message)
         )
    {
        QHash<QByteArray, QByteArray> entry;

        if (parseEntry(message, &entry)) {
            entryList->append(entry);
        }
    }
    ldap_msgfree(result);

    return true;
}

QByteArray LdapWrapper::connectionUrl()
{
    LDAPURLDesc ldapDescriptor;
    bzero(&ldapDescriptor, sizeof(ldapDescriptor));
    ldapDescriptor.lud_scheme = mScheme.data();
    ldapDescriptor.lud_host = mHost.data();
    ldapDescriptor.lud_scope = (int)mScope;

    return ldap_url_desc2str(&ldapDescriptor);
}

bool LdapWrapper::parseEntry(LDAPMessage *entry, Entry *resultEntry)
{
    BerElement *berPtr;
    for (
         char *name = ldap_first_attribute(mConnection, entry, &berPtr);
         name != nullptr;
         name = ldap_next_attribute(mConnection, entry, berPtr)
         )
    {
        struct berval **values = ldap_get_values_len(mConnection, entry, name);
        int count = ldap_count_values_len(values);
        for (int i = 0 ; i < count ; ++i) {
            resultEntry->insertMulti(name, values[i]->bv_val);
        }

        ldap_value_free_len(values);
        ldap_memfree(name);
    }
    ber_free(berPtr, 0);

    return true;
}

