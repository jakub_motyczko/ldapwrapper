#ifndef LDAPWRAPPER_H
#define LDAPWRAPPER_H

#include <QByteArray>

typedef struct ldap LDAP;
typedef struct ldapmsg LDAPMessage;

class QByteArray;

class LdapWrapper
{
public:
    LdapWrapper(const QByteArray &host);

    typedef QHash<QByteArray, QByteArray> Entry;

    enum class Scope {
        Main,
        One,
        Sub
    };

    /*!
     * \brief init - inilizes connection structure to LDAP
     * \param base e.g. "dc=domain,dc=com"
     * \param scope
     * \param scheme
     * \return
     */
    bool init(const QByteArray &base, const Scope scope = Scope::Main, const QByteArray &scheme = QByteArray("ldap"));

    /*!
     * \brief connect - connects/binds to LDAP service
     * \param login e.g. "user@domain.com"
     * \param password
     * \return
     */
    bool connect(const QByteArray &login, const QByteArray &password, bool *ok);

    bool disconnect();

    /*!
     * \brief search - performs search on LDAP and returns matching entries
     * \param filter e.g. "(sn=Surname)"
     * \param entryList  - output parameter for entries
     * \return
     */
    bool search(const QByteArray &filter, QVector<Entry> *entryList);

    /*!
     * \brief connectionUrl - builds connection URL required for LDAP initialization
     * \return
     */
    QByteArray connectionUrl();

protected:
    LDAP *mConnection;
    bool mConnected = false;

    /*!
     * \brief mLogin "user@domain.com"
     */
    QByteArray mLogin;
    QByteArray mPassword;

    /*!
     * \brief mHost e.g. "127.0.0.1"
     */
    QByteArray mHost;

    /*!
     * \brief mBase e.g. "dc=domain,dc=com"
     */
    QByteArray mBase;

    QByteArray mScheme = "ldap";

    Scope mScope = Scope::Main;

    bool parseEntry(LDAPMessage *entry, Entry *resultEntry);
};

#endif // LDAPWRAPPER_H
