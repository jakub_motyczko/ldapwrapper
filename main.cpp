#include <QCoreApplication>
#include <QDebug>

#include "src/LdapWrapper.h"

int main(int argc, char *argv[])
{
    qSetMessagePattern("[%{type}] %{time hh:mm:ss:zzz}\t%{threadid}\t%{file}:%{line}\n\t\t\t%{message}");

//command line for ldapsearch:
//get one user
//    ldapsearch -H  ldap://host:389 -D "username@domain.com" -W -b "dc=domain,dc=com" -s sub '(&(objectClass=user)(sAMAccountName=UserName)(memberof=CN=SomeCustomGroup,CN=Users,DC=domain,DC=com))'
//get all users
//    ldapsearch -H  ldap://host:389 -D "username@domain.com" -W -b "dc=domain,dc=com" -s sub '(&(objectClass=user))'
    LdapWrapper ldap("some.ldap.host.com");
    qDebug() << ldap.init("dc=motyczko,dc=pl", LdapWrapper::Scope::Sub);
    qDebug() << ldap.connect("jakub@motyczko.pl", "SomeCustomPassword");

    QVector<LdapWrapper::Entry> entryList;
    qDebug() << ldap.search("(sn=Motyczko)", &entryList);
    qDebug() << "Entries:\n" << entryList;
    qDebug() << ldap.disconnect();

    return 0;
}
